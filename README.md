### Env. vars

* `AMQP2ES_VERBOSITY` - verbosity level; can be `v`, `vv`, or `vvv`. defaults to `(empty)` (important messages to always display).
* `AMQP_DSN` to connect to AMQP (e.g. `amqp://guest:guest@localhost:5672/`)
* `AMQP_QUEUE_NAME` - the queue to fetch from.
* `ELASTICSEARCH_URL` to connect to ES cluster; e.g. `http://elasticsearch:9200`.
* `ES_INDEX_PREFIX` - the ES index name prefix; this will be prepended to `YYYY-MM-DD` based on log date; defaults to `sf4-app-`.
