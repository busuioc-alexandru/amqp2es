package main

import (
	"log"
	"sync"
	"time"
)

type InsertRate struct {
	count int
	s     sync.Mutex
}

func (i *InsertRate) Add(int int) {
	i.s.Lock()
	defer i.s.Unlock()
	i.count += int
}

func (i *InsertRate) Flush() int {
	i.s.Lock()
	defer func() {
		i.count = 0
		i.s.Unlock()
	}()
	return i.count
}

var insertRateCounter *InsertRate

func init() {
	insertRateCounter = &InsertRate{}
	go func() {
		var count int
		interval := time.Second
		for {
			select {
			case <-time.After(interval):
				if count = insertRateCounter.Flush(); count > 0 {
					log.Printf("inserted %d rows in last %s", count, interval.String())
				}
				break
			}
		}
	}()
}
