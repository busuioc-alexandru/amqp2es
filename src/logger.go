package main

import "log"

type Verbosity string

const Always Verbosity = ""
const Verbose Verbosity = "v"
const Verbose2 Verbosity = "vv"
const Verbose3 Verbosity = "vvv"

var verbosityLevels = map[Verbosity]struct{}{
	Always:   {},
	Verbose:  {},
	Verbose2: {},
	Verbose3: {},
}

type Logger struct {
	verbosity Verbosity
	loggerIo  *log.Logger
}

func NewLogger(loggerIo *log.Logger, verbosity Verbosity) *Logger {
	if _, ok := verbosityLevels[verbosity]; !ok {
		verbosity = Always
	}
	return &Logger{
		verbosity: verbosity,
		loggerIo:  loggerIo,
	}
}

func (l *Logger) GetLoggerIo() *log.Logger {
	return l.loggerIo
}

func (l *Logger) Println(verbosity Verbosity, v ...interface{}) {
	if !l.allowedVerbosity(verbosity) {
		return
	}
	l.loggerIo.Println(v...)
}

func (l *Logger) Printf(verbosity Verbosity, format string, v ...interface{}) {
	if !l.allowedVerbosity(verbosity) {
		return
	}
	l.loggerIo.Printf(format, v...)
}

func (l *Logger) allowedVerbosity(verbosity Verbosity) bool {
	return len(verbosity) <= len(l.verbosity)
}
