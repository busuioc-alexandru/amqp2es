package main

import (
	"bytes"
	"context"
	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esutil"
	"log"
	"os"
	"runtime"
	"time"
)

type Error struct {
	Msg string
}

type UnsuccessfulResponseError Error
type BadFormatResponseError Error

func (e UnsuccessfulResponseError) Error() string {
	return e.Msg
}

func (e BadFormatResponseError) Error() string {
	return e.Msg
}

var client *elasticsearch.Client
var bi esutil.BulkIndexer
var err error
var esIndexPrefix string
var bgCtxt context.Context

func init() {
	log.Println("Elasticsearch Client version:", elasticsearch.Version)
	client, err = elasticsearch.NewDefaultClient()
	if err != nil {
		panic(err)
	}
	log.Println(client.Info())
	if esIndexPrefix = os.Getenv("ES_INDEX_PREFIX"); esIndexPrefix == "" {
		esIndexPrefix = "sf4-app-"
	}

	// Create the BulkIndexer
	bi, err = esutil.NewBulkIndexer(esutil.BulkIndexerConfig{
		Index:         esIndexPrefix,    // The default index name
		Client:        client,           // The Elasticsearch client
		NumWorkers:    runtime.NumCPU(), // The number of worker goroutines
		FlushBytes:    4096 * 1e4,       // The flush threshold in bytes
		FlushInterval: 2 * time.Second,  // The periodic flush interval
		OnError: func(ctx context.Context, err2 error) {
			logger.Println(Always, "ERR(BulkIndexer):", err2.Error())
		},
	})
	if err != nil {
		panic(err)
	}

	bgCtxt = context.Background()
}

func BulkPublishToEs(
	index string,
	documentId string,
	message []byte,
	onSuccess func(ctx context.Context, item esutil.BulkIndexerItem, res esutil.BulkIndexerResponseItem),
	onFailure func(ctx context.Context, item esutil.BulkIndexerItem, res esutil.BulkIndexerResponseItem, err error),
) error {
	logger.Println(Verbose3, "bulk publish; index:", index, "; document id: ", documentId, "; message body: ", string(message))
	err = bi.Add(bgCtxt, esutil.BulkIndexerItem{
		Index: index,
		// Action field configures the operation to perform (index, create, delete, update)
		Action: "create",
		// DocumentID is the (optional) document ID
		DocumentID: documentId,
		// Body is an `io.Reader` with the payload
		Body: bytes.NewReader(message),

		// OnSuccess is called for each successful operation
		// OnSuccess: func(ctx context.Context, item esutil.BulkIndexerItem, res esutil.BulkIndexerResponseItem) {
		// 	atomic.AddUint64(&countSuccessful, 1)
		// },
		OnSuccess: onSuccess,

		// OnFailure is called for each failed operation
		// OnFailure: func(ctx context.Context, item esutil.BulkIndexerItem, res esutil.BulkIndexerResponseItem, err error) {
		// 	if err != nil {
		// 		log.Printf("ERROR: %s", err)
		// 	} else {
		// 		log.Printf("ERROR: %s: %s", res.Error.Type, res.Error.Reason)
		// 	}
		// },
		OnFailure: onFailure,
	})

	if err != nil {
		return err
	}

	return nil
}

func getEsIndexPrefix() string {
	return esIndexPrefix
}
