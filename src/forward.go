package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/elastic/go-elasticsearch/v7/esutil"
	"github.com/streadway/amqp"
	"gitlab.com/busuioc-alexandru/rmq-client"
	"log"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
)

var processMessage func(message amqp.Delivery, wg *sync.WaitGroup, isRetry bool)
var loggerIo *log.Logger
var logger *Logger

func init() {
	loggerIo = log.New(os.Stderr, "", log.LstdFlags)
	verbosity := os.Getenv("AMQP2ES_VERBOSITY")
	if verbosity == "" {
		verbosity = string(Always)
	}
	logger = NewLogger(loggerIo, Verbosity(verbosity))
	processMessage = func(message amqp.Delivery, wg *sync.WaitGroup, isRetry bool) {
		logger.Println(Verbose3, "start processing new message", string(message.Body))
		wg.Add(1)
		defer wg.Done()

		var logRecord map[string]interface{}
		var nackAndForget = func(message *amqp.Delivery) {
			_ = message.Nack(false, false)
		}
		var nackAndRequeueOne = func(message *amqp.Delivery) {
			_ = message.Nack(false, true)
		}

		if err := json.Unmarshal(message.Body, &logRecord); err != nil {
			log.Println(fmt.Errorf(" ! unmarshalling body: %s - %s", string(message.Body), err.Error()))
			nackAndForget(&message)
			return
		}

		dateFull := logRecord["@timestamp"].(string)
		dateYmd := strings.Split(dateFull, "T")[0]
		indexName := getEsIndexPrefix() + dateYmd
		documentId := getEsIndexPrefix() + dateFull

		onSuccess := func(currentMessage *amqp.Delivery) func(ctx context.Context, item esutil.BulkIndexerItem, res esutil.BulkIndexerResponseItem) {
			return func(ctx context.Context, item esutil.BulkIndexerItem, res esutil.BulkIndexerResponseItem) {
				if err := currentMessage.Ack(false); err != nil {
					logger.Println(Always, "E (ACK):", err)
				} else {
					insertRateCounter.Add(1)
				}
			}
		}(&message)
		onFailure := func(currentMessage *amqp.Delivery, wg *sync.WaitGroup) func(ctx context.Context, item esutil.BulkIndexerItem, res esutil.BulkIndexerResponseItem, err error) {
			return func(ctx context.Context, item esutil.BulkIndexerItem, res esutil.BulkIndexerResponseItem, err error) {
				// logger.Println(Always, "E (ESI):", err, res.Error, "ITEM:", item.Body)
				logger.Println(
					Always,
					"E (ESI): res.Error.Type:", res.Error.Type,
					"res.Error.Reason:", res.Error.Reason,
					"res.Error.Cause.Type:", res.Error.Cause.Type,
					"res.Error.Cause.Reason:", res.Error.Cause.Reason)
				if res.Error.Type == "version_conflict_engine_exception" {
					nackAndForget(currentMessage)
					logger.Println(Always, "E (ESI): the document exists, will discard")
					return
				}
				if isRetry {
					nackAndRequeueOne(currentMessage)
				} else {
					// type randomJson map[interface{}]randomJson
					jsonMap := make(map[string]interface{})
					if err := json.Unmarshal(currentMessage.Body, &jsonMap); err != nil {
						logger.Println(Always, err)
						nackAndForget(currentMessage)
						logger.Println(Always, "E (PARSE): message discarded", string(currentMessage.Body))
						return
					}
					if value, found := jsonMap["context"]; found {
						jsonContext, _ := json.Marshal(value)
						jsonMap["context"] = map[string][]byte{
							"json_context": jsonContext,
						}
						alteredMessageValue, _ := json.Marshal(jsonMap)
						alteredAmpDelivery := *currentMessage
						alteredAmpDelivery.Body = alteredMessageValue
						processMessage(alteredAmpDelivery, wg, true)
					} else {
						nackAndForget(currentMessage)
						logger.Println(Always, "E (PARSE): message discarded", string(currentMessage.Body))
						return
					}
				}
			}
		}(&message, wg)
		if err := BulkPublishToEs(indexName, documentId, message.Body, onSuccess, onFailure); nil != err {
			logger.Println(Always, "E (BPE):", err)
			nackAndRequeueOne(&message)
		}
	}
}

func main() {
	log.Println(fmt.Errorf(" ! starting the app..."))
	signalsIntTerm := make(chan os.Signal, 1)
	signal.Notify(signalsIntTerm, syscall.SIGINT, syscall.SIGTERM)
	amqpDsn := os.Getenv("AMQP_DSN") // amqp://guest:guest@localhost:5672/
	queueName := os.Getenv("AMQP_QUEUE_NAME")
	listenerCallback, processingWaitGroup := getQueueListenerCallback()
	client := rmq.New(queueName, amqpDsn, listenerCallback, loggerIo)
	wgForClose := &sync.WaitGroup{}
	wgForClose.Add(1)
	go func() {
		defer wgForClose.Done()
		select {
		case <-signalsIntTerm:
			if errMsg := client.Close(); nil != errMsg {
				log.Println("error closing the client", errMsg)
			}
			log.Println("wait for processing messages")
			processingWaitGroup.Wait()
		}
	}()
	if err := client.Stream(); nil != err {
		log.Fatal(err)
	}
	wgForClose.Wait()
}

func getQueueListenerCallback() (
	rmq.QueueListenerCallback,
	*sync.WaitGroup,
) {
	wg := &sync.WaitGroup{}
	return func(message amqp.Delivery) {
		go processMessage(message, wg, false)
	}, wg
}
