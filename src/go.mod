module amqp2es-fowarder

go 1.15

require (
	github.com/elastic/go-elasticsearch/v7 v7.10.0
	github.com/streadway/amqp v1.0.0
	gitlab.com/busuioc-alexandru/rmq-client v0.5.1
)
